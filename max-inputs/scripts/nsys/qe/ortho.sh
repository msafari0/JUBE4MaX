#!/bin/bash
if [[ $OMPI_COMM_WORLD_RANK == 0 ]]; then
        nsys  profile --trace=cuda,nvtx,mpi --capture-range=nvtx --nvtx-capture='ortho' --capture-range-end=repeat-shutdown[:10] --env-var=NSYS_NVTX_PROFILER_REGISTER_ONLY=0,NSYS_MPI_STORE_TEAMS_PER_RANK=1 --kill='none' $@
else
        $@
fi
