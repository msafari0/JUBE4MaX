#!/bin/bash
if [[ $OMPI_COMM_WORLD_RANK == #RANKID# ]]; then
        nsys  profile --trace=#EVENTS# #SWITCHES# --env-var=NSYS_NVTX_PROFILER_REGISTER_ONLY=0,NSYS_MPI_STORE_TEAMS_PER_RANK=1 --kill='none' $@
else
        $@
fi
