#!/bin/bash

np_node=$OMPI_COMM_WORLD_LOCAL_SIZE
rank=$OMPI_COMM_WORLD_LOCAL_RANK

block=$(( $np_node / 4 ))   # We have 4 GPUs

limit0=$(( $block * 1 ))
limit1=$(( $block * 2 ))
limit2=$(( $block * 3 ))
limit3=$(( $block * 4 ))

#-----------------

if [ $rank -lt $limit0 ]
then
   export CUDA_VISIBLE_DEVICES=0
 
elif [ $rank -lt $limit1 ]
then
   export CUDA_VISIBLE_DEVICES=1
 
elif [ $rank -lt $limit2 ]
then
   export CUDA_VISIBLE_DEVICES=2
else
   export CUDA_VISIBLE_DEVICES=3
fi

$@
